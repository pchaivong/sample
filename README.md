# Sample

This is only a simple springboot REST API for project `gateway` which is a
example implementation of netflix zuul

This service will provide only two api

- `/order`
- `/checkout`

The filtering logics are not include in here, but they will be in `gateway` project.


#### How to run

- Clone this repository `$ git clone https://gitlab.com/pchaivong/sample.git`
- Build and run `$ ./mvnw spring-boot:run`


#### Configuration

- Port `server.port`. Default is 8091 (if you change this, please ensure that you amend configuration in 
`gateway` as well)

##### Reference

- [Gateway project](https://gitlab.com/pchaivong/gateway.git)