package com.example.sample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@SpringBootApplication
public class SampleApplication {

    @RequestMapping(value = "/order")
    public String order(){
        return "Order Completed";
    }

    @RequestMapping(value="/checkout")
    public String checkout(){
        return "Checked out";
    }

	public static void main(String[] args) {
		SpringApplication.run(SampleApplication.class, args);
	}
}
